
# Tải ứng dụng 

[NODE JS](https://nodejs.org/dist/v20.12.0/node-v20.12.0-x64.msi)

[git](https://github.com/git-for-windows/git/releases/download/v2.44.0.windows.1/PortableGit-2.44.0-64-bit.7z.exe)

# Mở terminal 
```cmd 
B1 : cd D:\Vmix-DashBoard
B2 : git clone https://gitlab.com/mjssdn95/dinoesportsvmix
B3 : cd dinoesportsvmix
B4 : npm i 
B5 : npm start 
```

# Cổng Localhost 
```cmd 
http://localhost:36503/
```

# Setup OBS - Chọn Browser 
```
URL : http://localhost:36503/
width : 1600
height : 900
```