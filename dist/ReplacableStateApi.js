"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var events_1 = require("events");
var lol_esports_spectate_1 = require("lol-esports-spectate");
var ReplacableStateApi = /** @class */ (function (_super) {
    __extends(ReplacableStateApi, _super);
    function ReplacableStateApi() {
        return _super.call(this) || this;
    }
    ReplacableStateApi.prototype.connectToClient = function () {
        var _this = this;
        if (this.stateApi) {
            this.stateApi.removeAllListeners();
            this.stateApi.stop();
            if (this.stateApi.replay) {
                if (this.clientStateApi)
                    this.stateApi = this.clientStateApi;
                else {
                    this.clientStateApi = new lol_esports_spectate_1.ChampSelectStateApi();
                    this.stateApi = this.clientStateApi;
                }
            }
        }
        else {
            this.stateApi = new lol_esports_spectate_1.ChampSelectStateApi();
            this.clientStateApi = this.stateApi;
        }
        this.stateApi.on('newState', function (state) { return _this.onNewState(state); });
        this.stateApi.on('newPickOrder', function (state) { return _this.onNewPickOrder(state); });
        this.stateApi.on('championSelectStarted', function () { return _this.onChampionSelectStarted(); });
        this.stateApi.on('championSelectEnd', function () { return _this.onChampionSelectEnded(); });
    };
    ReplacableStateApi.prototype.loadReplay = function (replay_file) {
        var _this = this;
        if (this.stateApi) {
            this.stateApi.removeAllListeners();
            if (!this.stateApi.replay) {
                this.clientStateApi = this.stateApi;
            }
        }
        this.stateApi = new lol_esports_spectate_1.ChampSelectStateApi(true, replay_file);
        this.stateApi.on('newState', function (state) { return _this.onNewState(state); });
        this.stateApi.on('newPickOrder', function (state) { return _this.onNewPickOrder(state); });
        this.stateApi.on('championSelectStarted', function () { return _this.onChampionSelectStarted(); });
        this.stateApi.on('championSelectEnd', function () { return _this.onChampionSelectEnded(); });
    };
    ReplacableStateApi.prototype.onChampionSelectEnded = function () {
        this.emit('championSelectEnd');
    };
    ReplacableStateApi.prototype.onChampionSelectStarted = function () {
        this.emit('championSelectStarted');
    };
    ReplacableStateApi.prototype.onNewPickOrder = function (state) {
        this.emit('newPickOrder', state);
    };
    ReplacableStateApi.prototype.onNewState = function (state) {
        this.emit('newState', state);
    };
    ReplacableStateApi.prototype.getConnectionStatus = function () {
        return this.stateApi.getConnectionStatus();
    };
    return ReplacableStateApi;
}(events_1.EventEmitter));
exports["default"] = ReplacableStateApi;
//# sourceMappingURL=ReplacableStateApi.js.map