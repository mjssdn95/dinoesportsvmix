"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var ws_1 = __importDefault(require("ws"));
var ReplacableStateApi_1 = __importDefault(require("./ReplacableStateApi"));
var MyWebSocketServer = /** @class */ (function () {
    function MyWebSocketServer(port, host) {
        this.port = port;
        this.host = host;
        this.clients = [];
    }
    MyWebSocketServer.prototype.start = function () {
        var _this = this;
        var currentState = null;
        var currentPickOrder = null;
        var currentChampionSelectEnded = false;
        var currentChampionSelectStarted = false;
        this.api = new ReplacableStateApi_1["default"]();
        this.api.connectToClient();
        this.api.on('newState', function (state) { return currentState = state; });
        this.api.on('newPickOrder', function (state) { return currentPickOrder = state; });
        this.api.on('championSelectStarted', function () { currentChampionSelectStarted = true; currentChampionSelectEnded = false; });
        this.api.on('championSelectEnd', function () { currentChampionSelectEnded = true; currentChampionSelectStarted = false; });
        this.wsServer = new ws_1["default"].Server({ port: this.port, host: this.host });
        this.wsServer.on('connection', function (ws) {
            _this.clients.push(ws);
            // send current status
            if (currentChampionSelectStarted)
                ws.send(JSON.stringify({ "event": "championSelectStarted" }));
            if (currentState != null)
                ws.send(JSON.stringify({ "event": "newState", "data": currentState }));
            if (currentPickOrder != null)
                ws.send(JSON.stringify({ "event": "newPickOrder", "data": currentPickOrder }));
            if (currentChampionSelectEnded)
                ws.send(JSON.stringify({ "event": "championSelectEnded" }));
            var newState = function (state) {
                ws.send(JSON.stringify({ "event": "newState", "data": state }));
            };
            var championSelectStarted = function () {
                ws.send(JSON.stringify({ "event": "championSelectStarted" }));
            };
            var championSelectEnded = function () {
                ws.send(JSON.stringify({ "event": "championSelectEnded" }));
            };
            var newPickOrder = function (state) {
                ws.send(JSON.stringify({ "event": "newPickOrder", "data": state }));
            };
            _this.api.addListener("newState", newState);
            _this.api.addListener("championSelectStarted", championSelectStarted);
            _this.api.addListener("championSelectEnd", championSelectEnded);
            _this.api.addListener("newPickOrder", newPickOrder);
            ws.on('message', function (message) {
                _this.clients.forEach(function (client) {
                    client.send(message);
                });
            });
            ws.on('close', function () {
                _this.clients.splice(_this.clients.indexOf(ws), 1);
                _this.api.removeListener("newState", newState);
                _this.api.removeListener("championSelectStarted", championSelectStarted);
                _this.api.removeListener("championSelectEnd", championSelectEnded);
                _this.api.removeListener("newPickOrder", newPickOrder);
            });
        });
    };
    MyWebSocketServer.prototype.getRunningStatus = function () {
        if (!this.wsServer) {
            return "Stopped";
        }
        return "Running";
    };
    MyWebSocketServer.prototype.sendToAllClients = function (message) {
        this.clients.forEach(function (client) {
            client.send(message);
        });
    };
    MyWebSocketServer.prototype.getStateApi = function () {
        return this.api;
    };
    return MyWebSocketServer;
}());
exports["default"] = MyWebSocketServer;
//# sourceMappingURL=WebSocketServer.js.map