"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var electron_1 = require("electron");
var path = __importStar(require("path"));
var events_1 = __importDefault(require("events"));
var json8_merge_patch_1 = __importDefault(require("json8-merge-patch"));
var fs = __importStar(require("fs"));
var express_1 = __importDefault(require("express"));
var downloadImages_1 = require("./downloadImages");
var WebSocketServer_1 = __importDefault(require("./WebSocketServer"));
var express_serve_asar_1 = __importDefault(require("express-serve-asar"));
var WEBSERVER_PORT = 36501;
var WEBSOCKET_SERVER_PORT = 36502;
var server;
var serverSocket;
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var winConf;
var defaultConfig = {
    webserverPort: WEBSERVER_PORT,
    websocketServerPort: WEBSOCKET_SERVER_PORT
};
var defaultBrowserWindowOptions = {
    width: 1600,
    height: 900,
    webPreferences: {
        preload: path.join(__dirname, 'preload.js')
    },
    frame: true
};
var config = defaultConfig;
var webSocketServer = null;
function createConfigWindow() {
    winConf = new electron_1.BrowserWindow(defaultBrowserWindowOptions);
    // and load the index.html of the app.
    // winConf.loadFile('config.html')
    winConf.loadFile(path.join(__dirname, '../config.html'));
    // Open the DevTools.
    // win.webContents.openDevTools()
    // Emitted when the window is closed.
    winConf.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        winConf = null;
    });
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
electron_1.app.whenReady().then(function () {
    loadConfig();
    startWebsocketServer();
    updateImagesOnFirstStart();
    setupIpcMainHandlers();
    handleStartWebServer();
    createConfigWindow();
    electron_1.app.on('activate', function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (winConf === null) {
            createConfigWindow();
        }
    });
});
// Quit when all windows are closed.
electron_1.app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
electron_1.app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (winConf === null) {
        createConfigWindow();
    }
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
electron_1.ipcMain.on("open-overlay", function (event, arg) {
    electron_1.shell.openExternal("http://localhost:" + config.webserverPort);
});
var configEventEmitter = new events_1["default"]();
electron_1.ipcMain.on("newConfig", function (event, newConfig) {
    config = json8_merge_patch_1["default"].apply(config, newConfig);
    console.log("on newConfig", config);
    fs.writeFile(path.join(__dirname, "..", "config.json"), JSON.stringify(config), function (err) { console.log(err); });
    configEventEmitter.emit("newConfig", config);
});
configEventEmitter.addListener("newConfig", function (config) {
    webSocketServer.sendToAllClients(JSON.stringify({ "event": "newConfig", "data": config }));
});
function loadConfig() {
    try {
        var data = fs.readFileSync(path.join(__dirname, "..", "config.json"), { encoding: "utf8" });
        var newConfig = JSON.parse(data);
        config = json8_merge_patch_1["default"].apply(config, newConfig);
    }
    catch (err) {
        if (err === "ENOENT") {
            console.log("config.json not found, using default config");
        }
    }
}
function startWebsocketServer() {
    webSocketServer = new WebSocketServer_1["default"](config.websocketServerPort, '0.0.0.0');
    webSocketServer.start();
}
setInterval(function () {
    var serverStatus = {
        webSocketServer: webSocketServer.getRunningStatus(),
        webServer: serverSocket != null ? "Running" : "Stopped",
        leagueClient: webSocketServer.getStateApi().getConnectionStatus()
    };
    winConf.webContents.send("server-status", serverStatus);
}, 1000);
function setupIpcMainHandlers() {
    electron_1.ipcMain.handle('load-replay-dialog', handleOpenReplayDialog);
    electron_1.ipcMain.handle('update-images', downloadImages_1.checkForNewChampionImages);
    electron_1.ipcMain.handle('start-web-server', handleStartWebServer);
    electron_1.ipcMain.handle('stop-web-server', handleStopWebServer);
    electron_1.ipcMain.handle("connect-to-client", handleConnectToClient);
    electron_1.ipcMain.handle('get-config', handleGetConfig);
}
function handleGetConfig() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, config];
        });
    });
}
function handleOpenReplayDialog() {
    return __awaiter(this, void 0, void 0, function () {
        var _a, canceled, filePaths;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, electron_1.dialog.showOpenDialog({
                        properties: ['openFile']
                    })];
                case 1:
                    _a = _b.sent(), canceled = _a.canceled, filePaths = _a.filePaths;
                    if (canceled) {
                        return [2 /*return*/];
                    }
                    webSocketServer.getStateApi().loadReplay(filePaths[0]);
                    return [2 /*return*/, filePaths[0]];
            }
        });
    });
}
function handleConnectToClient() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            webSocketServer.getStateApi().connectToClient();
            return [2 /*return*/, true];
        });
    });
}
function handleStartWebServer() {
    var contentBase = path.join(__dirname, "..", "build");
    var middleware = (0, express_serve_asar_1["default"])(contentBase);
    server = (0, express_1["default"])();
    server.use(express_1["default"].static(path.join(__dirname, "..", "build")));
    server.use(middleware);
    server.get('/images/*', function (req, res) {
        res.sendFile(path.join(electron_1.app.getPath('userData'), "images", req.params[0]));
    });
    server.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, "..", "build", "index.html"));
    });
    serverSocket = server.listen(config.webserverPort, function () {
        console.log("server started on port " + config.webserverPort);
    });
    return "success";
}
function handleStopWebServer() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            serverSocket.close();
            return [2 /*return*/, "success"];
        });
    });
}
function updateImagesOnFirstStart() {
    var firstTimeFilePath = path.resolve(electron_1.app.getPath('userData'), '.first-time-huh');
    var isFirstTime;
    try {
        fs.closeSync(fs.openSync(firstTimeFilePath, 'wx'));
        isFirstTime = true;
    }
    catch (e) {
        if (e.code === 'EEXIST') {
            isFirstTime = false;
        }
        else {
            // something gone wrong
            return;
        }
    }
    if (isFirstTime) {
        (0, downloadImages_1.checkForNewChampionImages)();
    }
}
//# sourceMappingURL=main.js.map