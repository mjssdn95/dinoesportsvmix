var _a = require('electron'), contextBridge = _a.contextBridge, ipcRenderer = _a.ipcRenderer;
contextBridge.exposeInMainWorld('electronAPI', {
    getConfig: function () { return ipcRenderer.invoke('get-config'); },
    loadReplay: function () { return ipcRenderer.invoke('load-replay-dialog'); },
    updateImages: function () { return ipcRenderer.invoke('update-images'); },
    connectToClient: function () { return ipcRenderer.invoke('connect-to-client'); },
    sendNewConfig: function (newConfig) { return ipcRenderer.send('newConfig', newConfig); },
    openOverlay: function () { return ipcRenderer.send('open-overlay'); },
    onNewServerStatus: function (callback) { return ipcRenderer.on('server-status', callback); }
});
//# sourceMappingURL=preload.js.map